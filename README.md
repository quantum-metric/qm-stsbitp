Use this script in TamperMonkey.

It’s on by default all the time, and should only really do anything if the site you are viewing loads QMAPI.
Change the @match and @include values in the script if you want to lock it down. 
I’ve already added an exception for *.quantummetric.com* so it doesn’t load on our subs.

Works with the Injector plugin and production deployments of our tag/js

Current Features

- Css Blacklist
  - Adds a red border to any element that matches the CSS Blacklist for the script you are loading
  - Appends “*QMBL*” to SELECT OPTIONS that are blacklisted.
  - Seems to work with the “[selector] *” stuff, but not fully tested.

- Click Events
  -Adds a Blue Border to buttons and other elements that are configured for the page you are viewing

- Overlay containing the freshest functionality!
  - Toggles for CSS Blacklist and Click Events
  - Cart Value on the fly!
  - Session ID with new tab click directly to the sub/session you are working on
  - Integration opportunities
  - Competition callouts