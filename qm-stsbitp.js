 // ==UserScript==
// @name         STSBITP Script
// @version      0.2.9.4
// @description  TamperMonkey that drastically decreases the time it takes to configure a subscription for QuantumMetric
// @author       JT - jtaylor@quantummetric.com
// @match        *://*/*
// @include      *
// @exclude      *.quantummetric.com*
// @grant        none
// @run-at       document-end
// @updateURL    https://gitlab.com/quantum-metric/qm-stsbitp/raw/master/qm-stsbitp.js
// @downloadURL  https://gitlab.com/quantum-metric/qm-stsbitp/raw/master/qm-stsbitp.js
// ==/UserScript==

//Turn on debugging (where supported)
var apiDebug = false;

var getQuantumSession = function(){
                return window.QuantumMetricAPI.getSession();
}

var addGlobalStyle = function(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}

addGlobalStyle('[cssblacklist] { outline:2px dotted red; }');
addGlobalStyle('[regexblacklist] { outline:2px dotted orange; }');
addGlobalStyle('[quantumclickevent] { outline:2px dotted blue; }');
addGlobalStyle('[quantumlastclicked] { outline:2px dotted purple; }');
addGlobalStyle('[cssencryptlist] { outline:2px dotted green; }');

//addGlobalStyle('button.qmapibutton { all:initial; cursor: pointer;-webkit-appearance: button;font-size: 100%;margin: 0;vertical-align: baseline;line-height: normal;font-family: Roboto,Geneva,Tahoma,Arial,Helvetica Neue,Helvetica,sans-serif;color: #333;box-sizing: border-box;border-color: rgb(216, 216, 216) rgb(209, 209, 209) rgb(186, 186, 186);border-style: solid;border-width: 1px;padding: 1px 7px 2px;background-color: buttonface;-webkit-font-smoothing: antialiased;text-rendering: auto;color: initial;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;display: inline-block;text-align: start;margin: 0em;font: 400 11px system-ui;}');
//addGlobalStyle('button.qmapibutton { all:initial; }');

var getQMAPIData = function(request){
    if(request == 'cartValue'){
        return (window.QuantumMetricAPI.getCartValue()/100).toLocaleString("en-US", {style:"currency", currency:window.QuantumMetricAPI.targetCurrency});}
    if(request == 'cssblacklist'){
        return window.QuantumMetricAPI.getConfig().dataScrubBlackList;}
    if(request == 'quantumclickevents'){
        return window.QuantumMetricAPI.getConfig().eventDefinitions.events;}
    if(request == 'cssencryptlist'){
        return window.QuantumMetricAPI.getConfig().encryptScrubList;}
    if(request == 'lastClicked'){
        return window.QuantumMetricAPI.lastClicked;}
    if(request == 'regexblacklist'){
        return '/' + window.QuantumMetricAPI.getConfig().dataScrubRE.join('|') + '/i';
    }
    if(request == 'spinner'){
        return window.QuantumMetricAPI.getConfig().spinnerSelectorList;}
    if(request == 'norage') {
        return window.QuantumMetricAPI.getConfig().excludeRageRE;}
}

var cssBlacklist = function(state){
    if(state == 'on'){
        if(!!getQMAPIData('cssblacklist')){
            var values=getQMAPIData('cssblacklist');

            for(var a = 0; a < values.length; a++){
                //Adds a thin dotted red border to fields and other html elements that are being CSS Blacklisted
                for(var i=0, element=document.querySelectorAll(values[a]);i<element.length;i++){
                    if(!!element[i].nodeName.match(/OPTION/) && !element[i].innerText.match(/\*QMBL\*/)){
                        element[i].innerText = element[i].innerText + " *QMBL*";
                        var toolTipText = 'CSS matches - ' + values[a].replace(/"/g,'\'');;
                        element[i].setAttribute('whyIsThisMasked', toolTipText);
                    }
                    element[i].setAttribute('cssblacklist', 'true');
                    var toolTipText = 'CSS matches - ' + values[a].replace(/"/g,'\'');
                    element[i].setAttribute('whyIsThisMasked', toolTipText);
                }
            }
        }
    }

    if(state == 'off'){
        for(var i=0, element=document.querySelectorAll('[cssblacklist="true"]');i<element.length;i++){
            if(!!element[i].nodeName.match(/OPTION/) && !!element[i].innerText.match(/\*QMBL\*/)){
                element[i].innerText = element[i].innerText.split(' *QMBL*')[0];
            }
            element[i].removeAttribute('cssblacklist');
        }
    }
}

var quantumClickEvents = function(state) {
    if(!!getQMAPIData('quantumclickevents')){
        if(state == 'on'){

            for(var c=0, clickcss = "", e=getQMAPIData('quantumclickevents');c<e.length;c++){
                var re = new RegExp(e[c].u, 'g');
                if(e[c].x == "QCE" && !!document.URL.match(re)){
                    if(!!apiDebug){console.log('found a click event on this Page - ' + e[c].v.v[0].v[1].v[0]);}
                    clickcss = clickcss + e[c].v.v[0].v[1].v[0] + ",";
                }
            }

            //Adds a medium dotted blue border to buttons and other html elements that are configured to be Quantum Click Events
            if(!!clickcss){
                for(var ce=0, QCEselector=document.querySelectorAll(clickcss.substr(0,clickcss.length-1));ce<QCEselector.length;ce++){
                    QCEselector[ce].setAttribute('quantumclickevent', 'true');
                }
            }

        }

        if(state == 'off'){
            if(document.querySelectorAll('[quantumclickevent="true"]').length > 0){
                for(var i=0, element=document.querySelectorAll('[quantumclickevent="true"]');i<element.length;i++){
                    element[i].removeAttribute('quantumclickevent');
                }
            }
        }
    }
}

var cssEncryptList = function(state) {
    if(state == 'on'){
        if(!!getQMAPIData('cssencryptlist')){
            var values=getQMAPIData('cssencryptlist');
            //var cssel = "";
            //values.forEach(function(item){
            //    cssel = cssel + item + ",";
            //});
            //cssel = cssel.substr(0,cssel.length-1);

            for(var a = 0; a < values.length; a++){
                for(var i=0, element=document.querySelectorAll(values[a]);i<element.length;i++){
                    if(!!element[i].nodeName.match(/OPTION/) && !element[i].innerText.match(/\*QMBL-Enc\*/)){
                        element[i].innerText = element[i].innerText + " *QMBL-Enc*";
                        var toolTipText = 'CSS matches - ' + values[a].replace(/"/g,'\'');;
                        element[i].setAttribute('whyIsThisEncrypted', toolTipText);
                    }
                    element[i].setAttribute('cssencryptlist', 'true');
                    var toolTipText = 'CSS matches - ' + values[a].replace(/"/g,'\'');;
                    element[i].setAttribute('whyIsThisEncrypted', toolTipText);
                }
            }
        }
    }

    if(state == 'off'){
        for(var i=0, element=document.querySelectorAll('[cssencryptlist="true"]');i<element.length;i++){
            if(!!element[i].nodeName.match(/OPTION/) && !!element[i].innerText.match(/\*QMBL-Enc\*/)){
                element[i].innerText = element[i].innerText.split(' *QMBL-Enc*')[0];
            }
            element[i].removeAttribute('cssencryptlist');
        }
    }
}

var regexBlackList = function(state) {
    if(state == 'on'){
        var regexbl=getQMAPIData('regexblacklist');
        var l = regexbl.length;
        var re = new RegExp(regexbl.substr(1,l-2), 'gi');
        var regexblSplit = regexbl.substr(1,l-2).split('|');
        var qsa = document.querySelectorAll('input');

        for(var i = 0;i < qsa.length;i++){
            var test = 0;

                if(!!qsa[i].name && !!qsa[i].name.match(re, 'gi')){
                    test = test + 1;
                }
                if(!!qsa[i].id && !!qsa[i].id.match(re, 'gi')){
                    test = test + 2;
                }
                if(!!qsa[i].className && !!qsa[i].className.match(re, 'gi')){
                    test = test + 4;
                }
            if(test>0){
                qsa[i].setAttribute('regexblacklist', 'true');

                var whichAtt;
                if(test == 1){
                    whichAtt = 'name';}
                if(test == 2){
                    whichAtt = 'id';}
                if(test == 3){
                    whichAtt = 'name and id';}
                if(test == 4){
                    whichAtt = 'class';}
                if(test == 5){
                    whichAtt = 'name and class';}
                if(test == 6){
                    whichAtt = 'id and class';}
                if(test == 7){
                    whichAtt = 'name, id and class';}

                for(var a = 0; a < regexblSplit.length; a++){
                    var reg = new RegExp(regexblSplit[a], 'gi');
                    if(!!qsa[i].name.match(reg) || !!qsa[i].id.match(reg) || !!qsa[i].className.match(reg)){
                        var toolTipText = 'Regex matches ' + whichAtt + ' - /' + regexblSplit[a] + '/';
                        qsa[i].setAttribute('whyIsThisMasked', toolTipText);
                    }
                }
            }
        }
    }

    if(state == 'off'){
        for(var i=0, element=document.querySelectorAll('[regexblacklist="true"]');i<element.length;i++){
            element[i].removeAttribute('regexblacklist');
        }
    }
}

var qmSwitch = function(){
    var checked = this.checked;
    var id = this.id;

    if(!!checked){
        if(id == 'cssblacklistSwitch'){
            cssBlacklist('on');}
        if(id == 'clickeventsSwitch'){
            quantumClickEvents('on');}
        if(id == 'cssencryptSwitch'){
            cssEncryptList('on');}
        if(id == 'regexblacklistSwitch'){
            regexBlackList('on');}
    }else{
        if(id == 'cssblacklistSwitch'){
            cssBlacklist('off');}
        if(id == 'clickeventsSwitch'){
            quantumClickEvents('off');}
        if(id == 'cssencryptSwitch'){
            cssEncryptList('off');}
        if(id == 'regexblacklistSwitch'){
            regexBlackList('off');}
    }
}

var qmSessionControl = function(){
    var id = this.id;
    if(id == 'qmsessionstop'){
    window.QuantumMetricAPI.stopSession();
    console.log('stop session');}
    if(id == 'qmsessionstart'){
    window.QuantumMetricAPI.startSession();
    console.log('start session');}
    if(id == 'qmsessionnew'){
    window.QuantumMetricAPI.newSession();
    console.log('new session');}
    //setTimeout(sessionData(),500);
    }

var foo = function(){
    alert(this.id);
}

var getIntegrationPoints = function(){
    var intList = new Array();
    (!!window.newrelic) ? intList.push('New Relic') : false;
    (!!window.optimizely || checkLocalStorage("optimizely")) ? intList.push('Optimizely') : false;
    (!!checkCookies(';dtCookie=') || !!checkCookies('rxVisitor')) ? intList.push('Dynatrace') : false;
    (!!window.adobe && !!window.adobe.target) ? intList.push('Adode-target') : false;
    (!!window.utag_data || !!window.utag) ? intList.push('Tealium') : false;
    return intList;
}

var getCompetition = function(){
    var compList = new Array();
    (!!window._detector) ? compList.push('GlassBox') : false;
    (!!window.ClickTaleIsRecording || window.ClickTaleIsRecording()) ? compList.push('ClickTale') : false;
    (!!window.decibelInsight) ? compList.push('DecibelInsight') : false;
    (!!window.hj || !!window.hj.includedInSample) ? compList.push('HotJar') : false;
    (!!window.SessionCamRecorder) ? compList.push('SessionCam') : false;
    return compList;
}

var checkLocalStorage = function(pattern){
    var tf = false;
    Object.keys(window.localStorage).forEach(function(i){
        var re = new RegExp(pattern, 'g');
        (!!i.match(re)) ? tf = true : false;
    });
    return tf;
}

var checkCookies = function(pattern){
    var re = new RegExp(pattern, 'g');
    if(!!document.cookie.match(re)){
        return true;}
    else{
        return false;}
}

var isDescendant = function(parent, child) {
    if(child != null){
        var node = child.parentNode;
        while (node != null) {
            if (node == parent) {
                return true;
            }
            node = node.parentNode;
        }
    }
    return false;
}

var qmapiOverlay = function(){
    //QMAPIOverlay Header
    var header = document.createElement( 'div' );
    header.id = 'QMAPIOverlayheader';
    header.textContent = 'STSBITP Data Box';
    header.style.cssText = ''+
        'padding: 10px;'+
        'cursor: move;'+
        'z-index: 4001;'+
        'background-color: #2196F3;'+
        'color: #fff;'+
        'box-sizing: border-box;'+
        'font-family: Roboto,Geneva,Tahoma,Arial,Helvetica Neue,Helvetica,sans-serif;'+
        'font-size: 1em;'+
        'line-height: 1.4;'+
        'text-rendering: auto;'+
        '-webkit-font-smoothing: antialiased;';

    var minimizeButton = document.createElement('div');
    minimizeButton.id = 'minimizeQMPlugin';
    minimizeButton.textContent = '-'
    minimizeButton.style.cssText = 'position: fixed;'+
              'border: 1px solid white;'+
              'width: 2%;'+
              'margin-left: 0;'+
              'margin-top: -26px;'+
              'font-size: 22px;'+
              'font-weight: 800;'+
              'padding: 0;'+
              'cursor: pointer;';
    header.appendChild(minimizeButton);

    minimizeButton.addEventListener('click', function() {
      var wrapper = document.querySelector('#qmPlugWrapper');
      if(wrapper.style.visibility == 'inherit') {
        wrapper.style.visibility = 'hidden';
        wrapper.style.height = '0';
      } else {
        wrapper.style.visibility = 'inherit';
        wrapper.style.height = '100%';
      }
    });

    // container div for content
    var container = document.createElement('div');
    container.id = 'qmPlugWrapper'
    container.style.cssText = 'visibility: inherit;'

    //QMAPIOverlay Entire Box
    var box = document.createElement('div');
    box.id = 'QMAPIOverlay';
    box.style.cssText = 'all: initial;'+
        'position: absolute;'+
        'z-index: 4000;'+
        'top: '+
         (!!window.localStorage.getItem('qmOverlayTop') ? window.localStorage.getItem('qmOverlayTop') : '8px')+
        '; '+
        'left:'+
        (!!window.localStorage.getItem('qmOverlayLeft') ? window.localStorage.getItem('qmOverlayLeft') : '8px')+
        ';'+
        'background-color: #f1f1f1;'+
        'border: 1px solid #d3d3d3;'+
        'text-align: center;'+
        'font-family: Roboto,Geneva,Tahoma,Arial,Helvetica Neue,Helvetica,sans-serif;'+
        'box-sizing: border-box;'+
        'color: #333;'+
        'font-size: 1em;'+
        'line-height: 1.4;'+
        'text-rendering: auto;'+
        '-webkit-font-smoothing: antialiased;';

    var qmdata = document.createElement('div');
    qmdata.id = 'qmdata';
    qmdata.style.cssText = 'text-align: left; padding: 8px;';

    var qmoptions = document.createElement('div');
    qmoptions.id = 'qmoptions';
    qmoptions.style.cssText = 'text-align: left; padding: 8px;';

    var qmsession = document.createElement('div');
    qmsession.id = 'qmsession';
    qmsession.style.cssText = 'text-align: left; padding: 8px;';
    qmsession.innerHTML = '<a target="_blank" id="qmsessionlink"><span id="qmsessionid"></span></a>';

    var qmsessioncontrol = document.createElement('div');
    qmsessioncontrol.id = 'qmsessioncontrol';
    qmsessioncontrol.style.cssText = 'text-align: left; padding: 8px;';
    qmsessioncontrol.innerHTML =
        '<button class="qmapibutton" id="qmsessionstop"></button>'+
        '<button class="qmapibutton" id="qmsessionstart"></button>'+
        '<button class="qmapibutton" id="qmsessionnew">New Session</button></br>'+
        '<button class="qmapibutton" id="qmnewEvent" style="background: green; color: white; width: 55%; padding: 5px 15px; margin-top: 5px;">Event Creator</button>';

    var qmcartvalue = document.createElement('span');
    qmcartvalue.id = 'qmcartvalue';
    qmcartvalue.class = 'qmdata';
    qmcartvalue.style.cssText = 'text-align: left; padding: 8px;';


    var createOption = function(id, where, what, color){
        var input = document.createElement('input');
        input.type = 'checkbox';
        input.checked = true;
        input.id = id;
        input.class = "qmapiswitch"
        input.style.cssText = '-webkit-appearance: checkbox;padding: 8px;';

        var label = document.createElement('label');
        label.setAttribute('for', id);
        label.innerText = what + ' ';
        //label.style.cssText = 'display: inline;padding: 8px;';

        var legend = document.createElement('div');
        legend.style.cssText = 'outline:2px dotted '+color+';margin: 8px;';

        legend.appendChild(input);
        legend.appendChild(label);
        where.appendChild(legend);
        //where.appendChild(document.createElement('br'));
    }

    var qmintegrationpoints = document.createElement('div');
    qmintegrationpoints.id = 'qmintegrationpoints';
    qmintegrationpoints.style.cssText = 'text-align: left; padding: 8px;';

    //Build the overlay
    document.body.appendChild(box);
    box.insertBefore(header, box.firstChild);

    container.appendChild(qmdata);
    qmdata.appendChild(qmcartvalue);
    container.appendChild(qmoptions);
    container.appendChild(qmsession);
    container.appendChild(qmsessioncontrol);
    container.appendChild(qmintegrationpoints);

    box.appendChild(container);

    //Generate Options
    createOption('cssblacklistSwitch', qmoptions, 'CSS Blacklist', 'red');
    createOption('regexblacklistSwitch', qmoptions, 'Regex Blacklist', 'orange');
    createOption('clickeventsSwitch', qmoptions, 'Click Events', 'blue');
    createOption('lastclickedSwitch', qmoptions, 'Last Clicked', 'purple');
    createOption('cssencryptSwitch', qmoptions, 'CSS Encrypt', 'green');

    for(var i=0, s=getIntegrationPoints();i<s.length;i++){
        var integration = document.createElement('span');
        integration.id = s[i];
        integration.innerText = s[i];
        integration.class = 'qmintegration';
        integration.style.cssText = 'padding: 8px;';
        document.getElementById('qmintegrationpoints').appendChild(integration);
    };

    document.getElementById('cssblacklistSwitch').addEventListener('change', qmSwitch, false);
    document.getElementById('regexblacklistSwitch').addEventListener('change', qmSwitch, false);
    document.getElementById('clickeventsSwitch').addEventListener('change', qmSwitch, false);
    document.getElementById('cssencryptSwitch').addEventListener('change', qmSwitch, false);

    document.getElementById('qmsessionstop').addEventListener('click', qmSessionControl, false);
    document.getElementById('qmsessionstart').addEventListener('click', qmSessionControl, false);
    document.getElementById('qmsessionnew').addEventListener('click', qmSessionControl, false);
    document.getElementById('qmnewEvent').addEventListener('click', function () {showHover = true;document.querySelector("#minimizeQMPlugin").click();setTimeout(function () {initEventCreator();},500)}, false);



    // Make the DIV element draggable:
    dragElement(document.getElementById("QMAPIOverlay"));

    function dragElement(elmnt) {
        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        if (document.getElementById(elmnt.id + "header")) {
            // if present, the header is where you move the DIV from:
            document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
        } else {
            // otherwise, move the DIV from anywhere inside the DIV:
            elmnt.onmousedown = dragMouseDown;
        }

        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            // get the mouse cursor position at startup:
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // set the element's new position:
            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
            elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
            window.localStorage.setItem('qmOverlayTop', elmnt.style.top);
            window.localStorage.setItem('qmOverlayLeft', elmnt.style.left);
        }

        function closeDragElement() {
            // stop moving when mouse button is released:
            document.onmouseup = null;
            document.onmousemove = null;
        }
    }
}

var timers = function(){
    var waitSessionStatus = setInterval(function(){
        var parts = document.cookie.split('QuantumMetricEnabled=');
        if(parts.length == 2){
            var qmEnabled = parts.pop().split(';').shift();
            if(qmEnabled == 'false'){
                document.getElementById('qmsessionstop').innerText = 'Session Off';
                document.getElementById('qmsessionstop').style = 'color: red;';
                document.getElementById('qmsessionstart').innerText = 'Start Session';
                document.getElementById('qmsessionstart').removeAttribute('style');
            }else if(qmEnabled == 'true'){
                document.getElementById('qmsessionstop').innerText = 'Stop Session';
                document.getElementById('qmsessionstop').removeAttribute('style');
                document.getElementById('qmsessionstart').innerText = 'Session On';
                document.getElementById('qmsessionstart').style = 'color: green;';
            }
        }else if(!!getQuantumSession()){
            document.getElementById('qmsessionstop').innerText = 'Stop Session';
            document.getElementById('qmsessionstop').removeAttribute('style');
            document.getElementById('qmsessionstart').innerText = 'Session On';
            document.getElementById('qmsessionstart').style = 'color: green;';
        }
    }, 2000);

    var waitForCartValue = setInterval(function(){
        if(getQMAPIData('cartValue') != '$0.00' && getQMAPIData('cartValue') != '$NaN' && getQMAPIData('cartValue') != document.getElementById('qmcartvalue').innerText.split(' ')[2]){
            document.getElementById('qmcartvalue').innerText = 'cartValue: ' + getQMAPIData('cartValue');
        }
    },500);

    var waitForLastClicked = setInterval(function(){
        if(typeof getQMAPIData('lastClicked') === 'object'){
            if(!isDescendant(document.getElementById('QMAPIOverlay'),getQMAPIData('lastClicked'))){
                if(document.querySelector('[quantumlastclicked="true"]') != null){
                    document.querySelector('[quantumlastclicked="true"]').removeAttribute('quantumlastclicked');}
                getQMAPIData('lastClicked').setAttribute('quantumlastclicked', 'true');
            }
        }
    },500);

    var waitForSessionCookie = setInterval(function(){
        if(document.cookie.split('QuantumMetricSessionID=').length == 2){
            var qmCookie = getQuantumSession();
            if(qmCookie != document.getElementById('qmsessionid').innerText){
                document.getElementById('qmsessionid').innerText = qmCookie;
                document.getElementById('qmsessionlink').href = window.QuantumMetricAPI.getConfig().reportURL.split('-')[0] + '.quantummetric.com/#/users/search?qmsessioncookie=' + qmCookie;
            }
        }
    }, 1000);
}

var waitForQMAPI = setInterval(function() {
    //START of wait for QMAPI
    if(!!window.QuantumMetricAPI) {
        clearInterval(waitForQMAPI);

        cssBlacklist('on');
        quantumClickEvents('on');
        cssEncryptList('on');
        regexBlackList('on');
        qmapiOverlay();
        timers();

    }
}, 250);





//START OF EVENT CREATOR
var showHover = false;
function initEventCreator(){
  $("body").prepend("<h1 id='inSelectorMode' style='cursor: pointer; background: green; color: white; position: fixed; z-index: 99999999999999999999; opacity: 1; width: 20vw; text-align: center; height: 10vh; font-size: 3.5vh; top: 0; margin: 0; line-height: 9.5vh;'>Hover over a Selector</h1>");
//Get The Selector of the Highlighted Element
function getSelector(event, parentSelectorNeeded, selectorString, parentCounter) {
  var currentSelector = "";
  var selectorIdentifiedFlag = false;
  var returnSelector = "";
  if (!parentSelectorNeeded) {
    if (!!event.target.id) {
      selectorIdentifiedFlag = true;
      returnSelector = "#" + event.target.id;
      console.log("********RETURNED: " + returnSelector);
      console.log(document.querySelectorAll(returnSelector));
      showHover = false;
      eventWizard(returnSelector);
      return returnSelector;
    }
    else if (event.target.classList.length > 0 && !/(col)|(row)/g.test(event.target.classList[0]) && !selectorIdentifiedFlag) {
      selectorIdentifiedFlag = true;
      currentSelector = "." + event.target.classList[0];
      getSelector(event, true, currentSelector, 1);
    }
    else if (!!event.target.tagName && !selectorIdentifiedFlag) {
      currentSelector = event.target.tagName;
      getSelector(event, true, currentSelector, 1);
    }
  }
  //Find the Elements Parent (No ID Found Yet)
  else {
    var currentParent = "";
    for (i = 0; i < parentCounter; i++) {
      if (i == 0) {
        currentParent = event.target.parentNode;
      }
      else if (i == 1) {
        currentParent = event.target.parentNode.parentNode;
      }
      else if (i == 2) {
        currentParent = event.target.parentNode.parentNode.parentNode;
      }
      else if (i == 3) {
        currentParent = event.target.parentNode.parentNode.parentNode.parentNode;
      }
      else if (i == 4) {
        currentParent = event.target.parentNode.parentNode.parentNode.parentNode.parentNode;
      }
    }

    if (!!currentParent.id && !selectorIdentifiedFlag) {
      selectorIdentifiedFlag = true;
      currentSelector = "#" + currentParent.id;
      returnSelector = currentSelector + " " + selectorString;
      console.log("********RETURNED: " + returnSelector);
      showHover = false;
      eventWizard(returnSelector)
      console.log(document.querySelectorAll(returnSelector));
      return returnSelector;
    } else if (currentParent.classList.length > 0 && !/(col)|(row)|(width)/gi.test(currentParent.classList[0]) && !selectorIdentifiedFlag) {
      if (parentCounter == 5) {
        selectorIdentifiedFlag = true;
        returnSelector = "." + currentParent.classList[0] + " " + selectorString;
        console.log("********RETURNED: " + returnSelector);
        showHover = false;
        eventWizard(returnSelector)
        console.log(document.querySelectorAll(returnSelector));
        return returnSelector;
      } else {
        currentSelector = "." + currentParent.classList[0] + " " + selectorString;
        parentCounter = parentCounter + 1;
        getSelector(event, true, currentSelector, parentCounter);
      }
    } else if (!!currentParent.tagName && !selectorIdentifiedFlag) {
      if (parentCounter == 5) {
        selectorIdentifiedFlag = true;
        returnSelector = currentParent.tagName + " " + selectorString;
        console.log("********RETURNED: " + returnSelector);
        showHover = false;
        eventWizard(returnSelector)
        console.log(document.querySelectorAll(returnSelector));
        return returnSelector;
      } else {
        currentSelector = currentParent.tagName + " " + selectorString;
        parentCounter = parentCounter + 1;
        getSelector(event, true, currentSelector, parentCounter);
      }
    }
  }
}

//Change Background Color of Current Element being hovered over
document.querySelector("body").onmouseover = document.querySelector("body").onmouseout = handler;

function handler(event, eventCreatorFlag) {
  // getSelector(event,false, "", 0);
  if (event.type == 'mouseover' && showHover) {
    event.target.style.background = 'pink'
  }
  if (event.type == 'mouseout' && showHover) {
    event.target.style.background = ''
  }
}


//HTML & JS for the Event Wizard
function eventWizard(returnSelector) {
  $("#inSelectorMode").remove();
  $('body').append('<div id="eventWizard" style="display: block; background:rgba(0,0,0,.4); height:100%; position:fixed; text-align:center; top:0; width:100%; z-index:10000;"><span class="helper" style="display:inline-block; height:100%; vertical-align:middle;"></span> <div style="background-color: #fff; box-shadow: 10px 10px 60px #555; display: inline-block; height: auto; max-width: 551px; min-height: 100px; vertical-align: middle; width: 60%; position: relative; border-radius: 8px; padding: 15px 2%; text-align: left;font-size: 20px;"> <div id="popupCloseButton" style="background-color: #fff; border: 3px solid #999; border-radius: 50px; cursor: pointer; display: inline-block; font-family: arial; font-weight: bold; position: absolute; top: -20px; right: -20px; font-size: 25px; line-height: 30px; width: 30px; height: 30px; text-align: center;"> X</div> <h1 style="font-weight: 900; margin: 0 0 0.67em 0;">Event Wizard</h1> <form action=""> <div>Event Name: </div> <input id="eventName" type="text" style="width:100%" required> </input> <div style="padding-top: 15px">Event Description: </div> <textarea id="eventDescription" style="width:100%"> </textarea> <div style="padding-top: 15px">Event Type: </div> <select id="eventType"> <option value="QClickedEvent">Click Event</option> <option value="selectorTypeEvent">Selector Present</option> <option value="errorTypeEvent">Error Present</option> </select> <div style="padding-top: 15px">Selector Identified: </div> <input id="selectorForEvent" type="text" style="width:100%" required> </input><button type="button" id="showSelectors" style="padding: 10px 10px; background: #0909ff; color: white; cursor: pointer; float: right; height: 20px; line-height: 0px; border: solid 1px #0c0cf3; border-radius: 10px;">Show Selectors</button> <div style="padding-top: 15px">Page: </div> <input id="singleURL" type="text" style="width:100%; display: block;"> </input> <input id="everyURL" type="text" style="width:100%; display: none;" value=".*" disabled> </input> <div style="width:100%"> <input id="allURLs" type="checkbox" /> <span style="line-height: 37px;"> All Page\'s </span> </div> <div style="width:100%; margin-top: 20px;"> <button type="button" name="createEvent" id="createEvent" style="padding: 20px 20px; background: #07b207; color: white; cursor: pointer; float: right; height: 40px; line-height: 0px; border: solid 1px green; border-radius: 10px; outline: none;">Create Event</button> </div> </form> </div> </div>');
  document.querySelector("#selectorForEvent").value = returnSelector;
  document.querySelector("#singleURL").value = window.location.pathname;

  document.getElementById('showSelectors').addEventListener('click', qmShowSelectors, false);

  //Url Checkbox
  document.querySelector("#allURLs").addEventListener('click', function(event) {
    if (document.querySelector("#allURLs").checked) {
      document.querySelector("#singleURL").style.display = "none";
      document.querySelector("#everyURL").style.display = "block";
    } else {
      document.querySelector("#singleURL").style.display = "block";
      document.querySelector("#everyURL").style.display = "none";
    }
  });

  //Create Event for Download
  document.querySelector("#createEvent").addEventListener('click', function(event) {
    var eventName = "\"" + document.querySelector("#eventName").value + "\"";
    var eventDescription = "\"" + document.querySelector("#eventDescription").value + "\"";
    var eventType = "\"" + document.querySelector("#eventType").value + "\"";
    var eventSelector = "\"" + document.querySelector("#selectorForEvent").value + "\"";
    var eventURL = document.querySelector("#allURLs").checked ? "\".*\"" : "\"" + document.querySelector("#singleURL").value + "\"";
    console.log("eventUrl " + eventURL);
    var template = "";
    //Click Event
    if (eventType.indexOf("QClickedEvent") > -1) {
      template = '[{"id":1,"event":' + eventName + ',"abbreviation":"TBD","type":' + eventType + ',"enabled":1,"value":0,"flags":0,"is_conversion":0,"is_error":0,"is_order_number":0,"promoted_event_value":0,"multiple_in_hit":1,"multiple_in_session":1,"created":"2019-07-22T13:17:33.000Z","updated":"2019-08-14T12:20:58.000Z","is_regex_url":0,"display_in_ui":1,"is_numeric":0,"description":' + eventDescription + ',"config":{"u":' + eventURL + ',"i":1,"m":0,"s":1,"f":0,"v":{"t":"E","v":[{"t":"ValueClause","v":[{"t":"ElementClickedNode","v":[]},{"t":"Matches","v":[' + eventSelector + ']}]},{"t":"V","v":[""]}]},"x":"QCE"}}]'
    }
    //Selector Present - "t":"SelectorPresent"
    else if (eventType.indexOf("selectorTypeEvent") > -1) {
      template = '[ {"id":1, "event":' + eventName + ', "abbreviation":"TBD", "type":"QConversionCSSEvent", "enabled":1, "value":0, "flags":0, "is_conversion":0, "is_error":0, "is_order_number":0, "promoted_event_value":0, "multiple_in_hit":1, "multiple_in_session":2, "created":"2019-03-05T11:53:00.000Z", "updated":null, "is_regex_url":0, "display_in_ui":1, "is_numeric":0, "description":' + eventDescription + ', "config":{ "u":' + eventURL + ', "i":1, "m":1, "s":2, "f":0,"v":{ "t":"E", "v":[{"t":"SelectorPresent", "v":[' + eventSelector + ']}, {"t":"V", "v":[""]}]}, "x":"QCC", "evalAlways":true}}]'
    }
    //Error Present - "is_error":1
    else if (eventType.indexOf("errorTypeEvent") > -1) {
      template = '[{"id":1, "event":' + eventName + ', "abbreviation":"TBD", "type":"QConversionCSSEvent", "enabled":1, "value":0, "flags":0, "is_conversion":0, "is_error":1, "is_order_number":0, "promoted_event_value":0, "multiple_in_hit":0, "multiple_in_session":1, "created":"2019-06-28T11:17:39.000Z", "updated":null, "is_regex_url":0, "display_in_ui":1, "is_numeric":0, "description":' + eventDescription + ', "config":{ "u":' + eventURL + ', "i":1,"m":0,"s":1,"f":0,"v":{"t":"E","v":[{ "t":"SelectorPresent", "v":[' + eventSelector + ']}, {"t":"SelectorText","v":[' + eventSelector + ']}]},"x":"QCC","evalAlways":true}}]'
    }
    var fileName = document.querySelector("#eventName").value + ".json";
    download(fileName, template);
  });
}

//Show Selectors
function qmShowSelectors(){
  var qmSelectors = document.querySelector("#selectorForEvent").value;
  var css = qmSelectors + '{background: red !important;}',
    head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');
    style.id = "qmShowSelectorsStyle";
    head.appendChild(style);
    style.type = 'text/css';
    style.appendChild(document.createTextNode(css));
    $("#eventWizard").css("visibility","hidden");
    $("body").prepend("<h1 id='showEventCreator' style='cursor: pointer; background: green; color: white; position: fixed; z-index: 99999999999999999999; opacity: 1; width: 20vw; text-align: center; height: 10vh; font-size: 3.5vh; top: 0; margin: 0; line-height: 9.5vh;'>Show Event Creator</h1>");
    $("#showEventCreator").click(function () {
      $("#showEventCreator").remove();
      $('#qmShowSelectorsStyle').remove();
      $("#eventWizard").css("visibility","visible");
    });
}

//Download .json Function
function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

//Click Tracking
document.addEventListener('click', function(event) {
  try {
    console.log(event.target.id);
    if (!!event.target.id && event.target.id == "popupCloseButton") {
      $('#eventWizard').remove();
      $('#qmShowSelectorsStyle').remove();
      $("#showEventCreator").remove();
      document.querySelector("#minimizeQMPlugin").click();
      showHover = false;
    }
    else if (!!event.target.id && (event.target.id == "qmnewEvent" || event.target.id == "minimizeQMPlugin")) {

    }
    else if (!document.querySelector("#eventWizard") && !!showHover) {
      getSelector(event, false, "", 0);
    }
  } catch (err) {}
})


//Remove Click Handlers/Links from Page so you can click them and not leave the page
for (i = 0; i < document.querySelectorAll("a").length; i++) {
  document.querySelectorAll("a")[i].href = "#"
}
}
